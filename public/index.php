<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDFBills</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/mystyle.css">
</head>
<body>
    
    <div class="container">
        <h1>PDFBills</h1>

        <table class="table table-striped">
            <thead>
                <tr>    
                   <th>R.Nr.</th>
                   <th>Rechnung als PDF</th>
                   <th>Firma</th>
                   <th>Status</th> 
                </tr>
            </thead>
            <tbody id="bills">
                <tr id="billtable_loader">
                    <td colspan="4">
                    <div class="text-center">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                    </td>
                </tr>
               <!-- <tr>
                    <td>R0001</td>
                    <td><a href="pdfs/bill_12345.pdf">PDF öffnen</a></td>
                    <td>Firma XY</td>   
                    <td>nicht bezahlt</td>
                </tr>
                <tr>
                    <td>R0002</td>
                    <td><a href="pdfs/bill_12345.pdf">PDF öffnen</a></td>
                    <td>Firma XY</td>   
                    <td>nicht bezahlt</td>
                </tr>
-->

            </tbody>
        </table>

        <h2>Neue Rechnung</h2>
       <form action="" id="newbill">
            <div class="form-group">
                <label for="firma">Firma</label>
                <input type="text" class="form-control" id="firma" placeholder="Firmenname Kunde">
            </div>
           <button type="submit" class="btn btn-primary">Neue Rechnung speichern</button>
       </form>
    </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/myscript.js"></script>
</body>
</html>