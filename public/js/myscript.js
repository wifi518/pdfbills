function load_bills() {
    $.ajax({
        url:'../bin/ajax.loadbills.php',
        success:function( response ) {
            //alert( response );
            // Tabelle erzeugen...
            $( '#billtable_loader' ).hide();
            for ( let i in response.bills ) {
                $( '<tr>' )
                    .append( $('<td>').html( response.bills[i].rnr ) )
                    .append( $('<td>').append(
                        $('<a>' )
                            .attr('href','pdfs/' + response.bills[i].filename )
                            .attr( 'target', '_blank' )
                            .html( 'PDF öffnen' )
                    ) )
                    .append( $('<td>').html( response.bills[i].firma ) )
                    .append( $('<td>').html( 'nicht bezahlt') )
                    .appendTo( '#bills' )

            }


        }
    });
}

$( document ).ready( function() {
    load_bills();
});

function add_bill() {
    $.ajax({
        url:'../bin/ajax.addbill.php',
        method:'post',
        data:{ firma: $('#firma').val() },
        success:function( response ) {
            if ( response.result == 'error' ) {
                for ( let i in response.required ) {
                    $( '#' + response.required[i] ).addClass( 'is-invalid' );
                }
            } else {
                $( '#billtable_loader' ).show();
                $( '#bills tr:not(#billtable_loader)' ).remove();
                $( '#firma' ).val('').removeClass( 'is-valid' );
                load_bills();
            }
        }
    });
}

$( document ).on( 'submit', 'form#newbill', function(e) {
    e.preventDefault();
    add_bill();
})

$( document ).on( 'change keyup', 'input', function(e) {
    $(this).removeClass( 'is-invalid').addClass( 'is-valid' );
})