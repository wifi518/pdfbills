<?php
   namespace Bill;
  
   use Dompdf\Dompdf;
   use Dompdf\Options;
   use \PDO;

class PDFBills {
    public function __construct() {
        $this->db = new PDO( 
            'mysql:host=localhost;dbname=bill;charset=utf8',
            'root',
            ''
        );
    }

    function get_all_bills() {       
        $sql = "SELECT * FROM bills";
        $result = $this->db->query( $sql );
        $allbills =  $result->fetchAll(PDO::FETCH_CLASS);
        return $allbills;
    }
    
    function get_next_billnumber() {
        
        /*
        *   get max used billnumber
        */
        $sql = "SELECT rnr FROM bills ORDER BY rnr DESC LIMIT 1";
        $result = $this->db->query( $sql );
        $number =  $result->fetchAll(PDO::FETCH_CLASS);
    
        if ( count( $number ) > 0 ) {
            $lastnr = $number[0]->rnr; 
            //R0002 => R0003
            $lastnr = (int) substr( $lastnr, 1 );
            return 'R'.sprintf("%04d",$lastnr+1);
            //substr('0000' . ($lastnr+1), -4);
        } else {
            return 'R0001';
        }
    
    }
    
    function insert_bill_db( $filename, $firma, $billnumber ) {           
        $sql = "
            INSERT INTO bills (rnr,filename,date,firma)
            VALUES (:billnumber,:filename,:date,:firma);
        ";  
       return $this->db->prepare( $sql )->execute( array( 
            'billnumber' => $billnumber,
            'filename' =>  $filename,
            'date' => date('Y-m-d H:i:s', time() ),
            'firma' => $firma
        ));
    
    }    

    function make_bill( $firma ) {
        $options = new Options();
        $options->set('defaultFont', 'Helvetica');
        $dompdf = new Dompdf( $options );
        $next_nr = $this->get_next_billnumber();
        $filename = dirname(__FILE__) . '/../bin/template/bill_template.html' ;
        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        $contents = preg_replace(
            array( '/%Rechnungsnummer%/' ),
            array( $next_nr ),
            $contents
        );
        fclose($handle);
        $dompdf->loadHtml( $contents );
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $pdf_gen = $dompdf->output();
        $bill_filename = 'bill_'.time().'.pdf';
        if(file_put_contents('../public/pdfs/'.$bill_filename, $pdf_gen)){
            $this->insert_bill_db( $bill_filename, $firma, $next_nr );
        }
    }



}