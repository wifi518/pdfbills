<?php
namespace Bill;
use \PDO; // weil nicht im Time Namespace

class MySQL {
    private $dbhost;
    private $dbuser;
    private $dbname;
    private $dbpass;
    protected $dbconn;
    
    function __construct( $host, $user, $db, $pwd ) {
        $this->dbhost = $host;
        $this->dbuser = $user;
        $this->dbname = $db;
        $this->dbpass = $pwd;
        $this->dbconn = new PDO('mysql:host='.$this->dbhost.';dbname='.$this->dbname.';charset=utf8',$this->dbuser,$this->dbpass);
     }    

    function select_all( $table ) {
        $sql = "SELECT * FROM `".$table."`";
        $result = $this->dbconn->query( $sql );
        return $result->fetchAll( PDO::FETCH_CLASS );
    }

    function insert( $table, $values ) {
        $data = [];
        $allplaceholders = [];
        $allkeys = [];
        foreach ( $values as $k=>$v ) {
            $data[$k] = filter_var($v,FILTER_SANITIZE_STRING);
            $allkeys[] = $k;
            $allplaceholders[] = ':'.$k;
        }        
        $sql = "INSERT INTO `".$table."` (".implode(',',$allkeys).") VALUES (".implode(',',$allplaceholders).")";
        return $this->dbconn->prepare($sql)->execute($data);
    }

    function delete( $table, $where ) {
        $data = [];
        $allwhere = [];       
        foreach ( $where as $k=>$v ) {
            $data[$k] = filter_var($v,FILTER_SANITIZE_STRING);
            $allwhere[] = "".$k."='". $data[$k] ."'";            
        }        
        $sql = "DELETE FROM `".$table."` WHERE ".implode(' AND ',$allwhere)."";
        return $this->dbconn->prepare($sql)->execute($data);
    }



}