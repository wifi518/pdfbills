<?php
    require '../vendor/autoload.php';
   // require '../src/bill_functions.php';

    use Bill\PDFBills;
    $pdfbills = new PDFBills();

    sleep(2); 
    
    $response = new stdClass();
    $response->bills = $pdfbills->get_all_bills();

    header( 'Content-Type:application/json' );
    echo json_encode( $response );