<?php
    require '../vendor/autoload.php';
    //require '../src/bill_functions.php';
    use Bill\PDFBills;
    $pdfbills = new PDFBills();

    sleep(2); 
    
    $response = new stdClass();
   
    if ( empty( $_POST['firma'] ) ) {
        $response->result = 'error';
        $response->required = ['firma'];
    } else {
        //require 'makebill.php'; // erzeugt Rechnung und fügt Zeile in DB ein
        $pdfbills->make_bill( $_POST['firma'] );
        $response->result = 'success';
    } 


    header( 'Content-Type:application/json' );
    echo json_encode( $response );